import io.gitlab.arturbosch.detekt.Detekt
import org.jetbrains.kotlin.gradle.dsl.JvmTarget
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm")
    id("com.google.devtools.ksp").version("1.9.22-1.0.17")
    id("io.gitlab.arturbosch.detekt")
    id("com.github.johnrengelman.shadow")
    `maven-publish`
}

val ciTag: String? = System.getenv("CI_COMMIT_TAG")

group = "com.gitlab.cromefire"
val snapshotVersion = "0.3.4"
version = if (!ciTag.isNullOrBlank() && ciTag.startsWith("v")) {
    ciTag.substring(1)
} else {
    "$snapshotVersion-SNAPSHOT"
}
base.archivesName.set("detekt-gitlab-report")

repositories {
    mavenCentral()
}

tasks.withType(KotlinCompile::class.java) {
    compilerOptions {
        jvmTarget.set(JvmTarget.JVM_1_8)
    }
}

dependencies {
    implementation("io.gitlab.arturbosch.detekt:detekt-api:1.23.5")

    implementation("com.squareup.moshi:moshi-kotlin:1.15.1")
    ksp("com.squareup.moshi:moshi-kotlin-codegen:1.15.1")

    testImplementation("io.gitlab.arturbosch.detekt:detekt-test:1.23.5")

    detektPlugins(project(project.path))
    detektPlugins("io.gitlab.arturbosch.detekt:detekt-formatting:1.23.5")
}

tasks {
    shadowJar {
        dependencies {
            exclude {
                it.moduleGroup == "io.gitlab.arturbosch.detekt"
            }
        }

        archiveClassifier.set("plugin")
        isPreserveFileTimestamps = false
        isReproducibleFileOrder = true
    }

    withType<Detekt> {
        basePath = rootDir.toString()

        reports {
            custom {
                reportId = "DetektGitlabReport"
                // This tells detekt, where it should write the report to,
                // you have to specify this file in the gitlab pipeline config.
                outputLocation.set(file(layout.buildDirectory.file("reports/detekt/gitlab.json")))
            }
        }
    }
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
    withSourcesJar()
}

detekt {
    buildUponDefaultConfig = true
    config.from("$rootDir/.config/detekt.yml")
}

// Fix for https://github.com/johnrengelman/shadow/issues/651
components.withType(AdhocComponentWithVariants::class.java).forEach { c ->
    c.withVariantsFromConfiguration(project.configurations.shadowRuntimeElements.get()) {
        skip()
    }
}

publishing {
    publications {
        register<MavenPublication>("maven") {
            artifactId = "detekt-gitlab-report"

            from(components["java"])

            pom {
                licenses {
                    license {
                        name.set("The Apache License, Version 2.0")
                        url.set("https://www.apache.org/licenses/LICENSE-2.0.txt")
                    }
                }
                developers {
                    developer {
                        id.set("cromefire_")
                        name.set("Cromefire_")
                        email.set("cromefire_@outlook.com")
                    }
                }
                scm {
                    connection.set("scm:git:git://gitlab.com/cromefire/detekt-gitlab-report.git")
                    developerConnection.set("scm:git:ssh://git@gitlab.com/cromefire/detekt-gitlab-report.git")
                    url.set("https://gitlab.com/cromefire/detekt-gitlab-report")
                }
            }
        }
    }
    repositories {
        maven("https://gitlab.com/api/v4/projects/25796063/packages/maven") {
            name = "gitlab"

            credentials(HttpHeaderCredentials::class.java) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN") ?: ""
            }
            authentication {
                register("header", HttpHeaderAuthentication::class.java)
            }
        }
    }
}
