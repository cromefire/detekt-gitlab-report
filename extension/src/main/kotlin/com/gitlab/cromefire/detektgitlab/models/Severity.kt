package com.gitlab.cromefire.detektgitlab.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = false)
enum class Severity {
    @Json(name = "info")
    Info,

    @Json(name = "minor")
    Minor,

    @Json(name = "major")
    Major,

    @Json(name = "critical")
    Critical,

    @Json(name = "blocker")
    Blocker,
}
