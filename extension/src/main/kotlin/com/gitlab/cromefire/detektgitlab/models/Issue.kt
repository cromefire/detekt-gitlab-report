package com.gitlab.cromefire.detektgitlab.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * https://github.com/codeclimate/platform/blob/master/spec/analyzers/SPEC.md#issues
 */
@JsonClass(generateAdapter = true)
data class Issue(
    /**
     * A unique name representing the static analysis check that emitted this issue.
     */
    @Json(name = "check_name")
    val checkName: String,
    /**
     * A description of the code quality violation.
     */
    val description: String,
    /**
     * At least one category indicating the nature of the issue being reported.
     */
    val categories: List<String>,
    /**
     * Location of the violation.
     */
    val location: Location,
    /**
     * Location of the violation.
     */
    val otherLocations: List<Location>,
    /**
     * An integer indicating a rough estimate of how long it would take to resolve the reported issue.
     */
    @Json(name = "remediation_points")
    val remediationPoints: Int,
    /**
     * A severity string.
     */
    val severity: Severity,
    /**
     * A unique fingerprint to identify the code quality violation. For example, an MD5 hash.
     */
    val fingerprint: String,
    /**
     * Must always be "issue".
     */
    val type: String = "issue"
)
